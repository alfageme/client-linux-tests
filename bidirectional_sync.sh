#!/usr/bin/env bash
set -ev

export RESULT="$(sed 's/:/_/g' <<<$CI_BUILD_NAME)"

mkdir -p syncdir/$CI_PIPELINE_ID && cd syncdir
# Get a 600x400 cat image to be uploaded to the server
wget -O $RESULT.jpeg  http://lorempixel.com/600/400/cats

# Query the server capabilities and print them on STDOUT
wget -q -O - --no-check-certificate $SERVER/status.php 2>&1

# Perform a full bidirectional sync of the contents of the server
owncloudcmd . $SERVER &> $CI_PIPELINE_ID/$RESULT.txt
# Re-run command-line client to upload its own logs
owncloudcmd --silent . $SERVER

# Remove sensitive data in the saved artifact and copy to the root
sed "s/$URL/SERVER_URL/g" $CI_PIPELINE_ID/$RESULT.txt > ../$RESULT.txt 