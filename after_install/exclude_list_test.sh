#!/usr/bin/env bash
set -e

# The shortname is the one displayed by --version flag:
APPLICATION_SHORTNAME="$($APPLICATION_EXECUTABLE'cmd' --version | awk 'NR==1{print $1}' | sed 's/cmd$//g')"
# Get the version (M.m) from the cmd client:
VERSION="$($APPLICATION_EXECUTABLE'cmd' --version | awk 'NR==1{print substr($3,1,3)}')"

# For legacy's sake we need one more check in the mainstream theme:
# ref. https://github.com/owncloud/client/blob/master/OWNCLOUD.cmake#L1
if [ "${APPLICATION_SHORTNAME,,}" == "owncloud" ] && [ "$APPLICATION_SHORTNAME" != "ownCloud" ]; 
then 
    echo -e "❌\033[0;31m - ownCloud's legacy shortname was not respected.\033[0m"
    exit 1
fi

# Check if the the sync-exclude.lst is placed where it should:
EXCLUSION_LIST="/etc/$APPLICATION_SHORTNAME/sync-exclude.lst"

if [ -f "$EXCLUSION_LIST" ];
then 
    echo -e "✅\033[0;32m - The exclusion list was found in the expected location.\033[0m"
    # Additionally, get autorative version of the exclusion list from GitHub:
    if wget -q https://raw.githubusercontent.com/owncloud/client/$VERSION/sync-exclude.lst; then
        # ... and compare with the one installed:
        if ! diff $EXCLUSION_LIST sync-exclude.lst; then echo "\033[1;33m* Autorative version of sync-exclude differs.\033[0m"; fi
    fi
else
    echo -e "❌\033[0;31m - Exclusion list not placed in the right dir.\033[0m"
    exit 1
fi

