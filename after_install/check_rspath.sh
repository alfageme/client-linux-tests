#!/usr/bin/env bash
set -e

#  Sanity check to verify the linked Qt dependency does not match a different 
#  Qt version - Qt 5.6.2 
#   TODO: should we include verifications for libqtcore4
#   see err.ref. https://gitlab.com/alfageme/client-linux-tests/-/jobs/27943635 

if [ $(ldd -v '/usr/bin/'$APPLICATION_EXECUTABLE | grep '/usr/lib/x86_64-linux-gnu/libQt5\|/usr/lib64/libQt5' | wc -l) -ne 0 ];
then
    echo -e "❌\033[0;31m - Client is using system's Qt.\033[0m"
    exit 1
else 
    echo -e "✅\033[0;32m - Client is using its bundled Qt version.\033[0m"
fi