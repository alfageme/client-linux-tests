#!/usr/bin/env bash
set -e

dnf install -y 'dnf-command(config-manager)'
dnf config-manager --add-repo $REPOSITORY