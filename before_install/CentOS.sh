#!/usr/bin/env bash
set -e

yum install -y wget
cd /etc/yum.repos.d/
wget $REPOSITORY

# Only needed for RHEL installations:
if [ "$0" == "before_install/RHEL.sh" ]; then
    wget https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
    yum install -y epel-release-latest-7.noarch.rpm
    yum install -y python34
fi