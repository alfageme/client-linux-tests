#!/usr/bin/env bash
set -e

wget $REPOSITORY_KEY
apt-key add - < Release.key
sh -c "echo 'deb $REPOSITORY /' > /etc/apt/sources.list.d/$APPLICATION_EXECUTABLE-client.list"
apt-get update